package at.ac.tuwien.sample.consumer;

import at.ac.tuwien.sample.api.EchoService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class ConsumerActivator implements BundleActivator {

    private EchoServiceConsumer _consumer;
    private ServiceTracker<EchoService, EchoService> _tracker;


    @Override
    public void start(BundleContext bundleContext) throws Exception {
        _tracker = new ServiceTracker<EchoService, EchoService>(bundleContext, EchoService.class, null);
        _tracker.open();
        _consumer = new EchoServiceConsumer(_tracker);
        _consumer.start();
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        _consumer.requestShutdown();
        _consumer.interrupt();
        _tracker.close();
    }
}
