package at.ac.tuwien.sample.consumer;

import at.ac.tuwien.sample.api.EchoService;
import org.osgi.util.tracker.ServiceTracker;

import java.util.Random;

public class EchoServiceConsumer extends Thread {

    private ServiceTracker<EchoService, EchoService> _serviceTracker;
    private volatile boolean _shutdownRequested = false;
    private Random _rng = new Random();

    public EchoServiceConsumer(ServiceTracker<EchoService, EchoService> serviceTracker) {
        _serviceTracker = serviceTracker;
    }

    @Override
    public void run() {
        int msgCtr = 1;

        while (!_shutdownRequested) {

            EchoService service = null;

            try {
                // wait infinitely until service is available
                service = _serviceTracker.waitForService(0);

                if (service != null) {
                    // a echo service is available - use it
                    String msg = String.valueOf(_rng.nextInt(1000));
                    System.out.println("consumer: sending   \"" + msg + "\"");
                    System.out.println("consumer: receiving \"" + service.echo(msg) + "\"");
                    msgCtr++;
                }

                // wait 5 seconds until
                sleep(5000);
            }
            catch (InterruptedException e) {
                // ignore and safely exit on shutdownRequested flag
            }
        }
    }

    public void requestShutdown() {
        _shutdownRequested = true;
    }
}
