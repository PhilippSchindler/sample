package at.ac.tuwien.sample.producer;

import at.ac.tuwien.sample.api.EchoService;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EchoServiceImpl implements EchoService {

    private final SimpleDateFormat _timeFormat = new SimpleDateFormat("HH:mm:ss");
    private int _requestCount;

    public void start() {
        _requestCount = 0;
        System.out.println("producer: service started");
    }

    public void shutdown() {
        System.out.println("producer: service stopped");
    }

    @Override
    public String echo(String message) {
        _requestCount++;
        System.out.println("producer: request received (" + _requestCount +" in total), message: " + message);
        return _timeFormat.format(new Date()) + ":" + message;
    }

}
