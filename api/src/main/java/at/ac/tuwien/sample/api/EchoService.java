package at.ac.tuwien.sample.api;

public interface EchoService {

    public String echo(String message);

}
